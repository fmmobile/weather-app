
import axios from "axios"

class Global {
    static getRequest(url:any) {
        return new Promise(resolve => {
            axios.get(url)
                .then((res:any) => {
                    // console.log(res, 'res')
                    resolve(JSON.stringify(res.data))
                })
                   .catch((err:any) => {
                    console.log(err, 'err')
                    resolve(err)
                })
        })
    }
    static  apiClient = axios.create({
        responseType: 'json',
        headers: {
          'Content-Type': 'application/json'
        }
      });
   
}
export default Global