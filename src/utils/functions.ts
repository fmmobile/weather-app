import Moment from 'moment'

export const kelvinToCelsius = (temp:any) => {
    return `${Math.round(temp - 273.15)}°`
}

export const roundCelsius = (temp:any) => {
    return `${Math.round(temp)}°`
}

export const moment12Hour = (dt:any) => {
    return Moment.unix(dt).format('h:mma')
}

export const momentHourOnly = (dt:any) => {
    return Moment.unix(dt).format('ha')
}

export const momentDay = (dt:any) => {
    return Moment.unix(dt).format('ddd')
}

export const getWeatherIcon = (icon:any) => {
    return `http://openweathermap.org/img/wn/${icon}@2x.png`
}