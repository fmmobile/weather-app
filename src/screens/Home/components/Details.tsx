import React  from 'react'
import {
    StyleSheet,
    View,
} from 'react-native'

import { CdIcon } from '../../../components/Card/index'
import { windowWidth } from '../../../utils/constants'
import { moment12Hour } from '../../../utils/functions'

interface props {
    children?:React.ReactNode,
    sunrise?: React.ReactNode,
    wind?:React.ReactNode,
    sunset?:React.ReactNode,
    humidity?:React.ReactNode,
      }
 const HmDetails = ( props : props) =>{
    const cardWidth = (windowWidth - 32 - 24) / 4
    return (
        <View style={styles.container}>
            <CdIcon
                containerStyle={{ width: cardWidth }}
                iconName='sunrise'
                title='Sunrise'
                value={moment12Hour(props.sunrise)}
            />
            <CdIcon
                containerStyle={{ width: cardWidth }}
                iconName='sunset'
                title='Sunset'
                value={moment12Hour(props.sunset)}
            />
            <CdIcon
                containerStyle={{ width: cardWidth }}
                iconName='wind'
                title='Wind'
                value={`${props.wind} m/s`}
            />
            <CdIcon
                containerStyle={{ width: cardWidth }}
                iconName='droplet'
                title='Humidity'
                value={`${props.humidity}%`}
            />
        </View>
    )

}

const styles = StyleSheet.create({
    container: {
        paddingHorizontal: 16,
        paddingVertical: 8,
        flexDirection: 'row',
        width: '100%',
        justifyContent: 'space-between',

    }
})

export default HmDetails