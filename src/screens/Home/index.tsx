import React, { Component } from "react";
import { StatusBar, Alert, StyleSheet, Platform, View, SafeAreaView, Switch, TouchableOpacity, Button } from "react-native";
import { observer, inject } from "mobx-react";
import { observable } from "mobx";
import HomeStore from "../../store/HomeStore";
import { ImContainer } from '../../components/Container'
import { IdTab } from '../../components/Indicator'
import { BxMessage } from '../../components/Box'
import { HmCurrent, HmDetails, HmHourly, HmDaily } from './components'
import { roundCelsius, getWeatherIcon } from '../../utils/functions'
import RNLocation from 'react-native-location'

interface props {
  navigation: any, homeStore: HomeStore;
  weather?: any, index?: any,
}
interface state {
  indicatorIndex?: number, isMood: boolean, message: string
}

@inject("homeStore")
@observer class Home extends Component<props, state> {

  constructor(props: any) {
    super(props);
    this.state = ({ indicatorIndex: 0, isMood: false, message: '' })
  }

  static navigationOptions = {
    title: "Home"
  };
  componentDidMount = () => {

    this.setState({ message: 'Getting current location' })
    this.requestLocationPermission()
  }
  requestLocationPermission = () => {
    RNLocation.requestPermission({
      ios: "whenInUse",
      android: {
        detail: "fine"
      }
    })
      .then(granted => {
        console.log('granted', granted)
        if (granted) {
          this.configureLocation()
        }
        else {
          this.alertLocationPermission()
        }
      })
  }

  alertLocationPermission = () => {
    Alert.alert(
      "Alert",
      "Location permission denied. Go to Settings to allow location permission.",
      [
        { text: "OK", onPress: () => this.requestLocationPermission() }
      ],
      { cancelable: false }
    )
  }

  configureLocation = () => {
    if (Platform.OS === 'ios') {
      RNLocation.configure({
        distanceFilter: 100,
        desiredAccuracy: {
          ios: "best",
        },
      })
      this.getLatestLocation()
    }
    else {
      RNLocation.configure({
        distanceFilter: 100,
        desiredAccuracy: {
          android: "highAccuracy"
        },
        androidProvider: "auto",
        interval: 5000,
        fastestInterval: 10000,
        maxWaitTime: 5000,
      })
        .then(_ => {
          console.log('configure SUCCESS')
          this.getLatestLocation()
        })
        .catch(error => {
          console.log('configure ERROR', error)
          this.alertLocationStatus()
        })
    }
  }

  alertLocationStatus = () => {
    Alert.alert(
      "Alert",
      "Location is disabled. For accurate result, please turn on device Location.",
      [
        { text: "OK", onPress: () => this.configureLocation() }
      ],
      { cancelable: false }
    )
  }

  getLatestLocation = () => {
    RNLocation.getLatestLocation({ timeout: 30000 })
      .then(latestLocation => {
        console.log('latest location', latestLocation)
        if (latestLocation) {
          const latitude = latestLocation.latitude
          const longitude = latestLocation.longitude
          this.getWeatherOneCall(latitude, longitude)
        }
        else {
          this.setState({ message: 'Retrying to get current location' })
          this.requestLocationPermission()
        }
      })
  }

  getWeatherOneCall = (lat: any, lon: any) => {
    const { homeStore } = this.props;
    this.setState({ message: 'Fetching current weather in your location' })

    homeStore.getWeather(lat, lon)
  }
  render() {
    const { homeStore } = this.props;
    const { indicatorIndex, isMood, message } = this.state;
    return (
      <ImContainer isMood={isMood} >
        <StatusBar translucent backgroundColor="transparent" />
        <SafeAreaView style={styles.container}>
          {
            homeStore?.weather.current == null ?
              <BxMessage message={message} />
              :
              <View style={styles.container}>
                <View style={styles.switch}>
                  <Switch
                    trackColor={{ false: "#767577", true: "#EAEFF2" }}
                    thumbColor={isMood ? "#000" : "#f4f3f4"}
                    ios_backgroundColor="#3e3e3e"
                    onValueChange={(e) => this.setState({ isMood: e })}
                    value={isMood}
                  /></View>
                <HmCurrent
                  image={getWeatherIcon(homeStore?.weather.current.weather[0].icon)}
                  location={homeStore?.weather.timezone}
                  description={homeStore?.weather.current.weather[0].main}
                  temperature={roundCelsius(homeStore?.weather.current.temp)}
                />
                <View>
                  <HmDetails
                    sunrise={homeStore?.weather.current.sunrise}
                    sunset={homeStore?.weather.current.sunset}
                    wind={homeStore?.weather.current.wind_speed}
                    humidity={homeStore?.weather.current.humidity}
                  />
                  <IdTab
                    titles={['Today', 'Next 7 Days']}
                    onChange={(index: number) => { this.setState({ indicatorIndex: index }) }}
                    index={indicatorIndex}
                  />
                  {
                    this.state.indicatorIndex == 0 ?
                      <HmHourly
                        data={homeStore?.weather.hourly.slice(0, 24)}
                      />
                      :
                      <HmDaily
                        data={homeStore?.weather.daily.slice(1, 8)}
                      />
                  }
                </View>
                <View style={styles.hight} />
              </View>
          }

        </SafeAreaView>
      </ImContainer>
    );
  }
}
const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  hight: { height: 12 },
  switch: { flexDirection: 'row-reverse', paddingLeft: 10, paddingTop: 30 }
})

export default Home;
