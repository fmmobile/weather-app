import React, { Component } from 'react';
import { Text, StyleSheet, View, ImageBackground } from 'react-native';

import { StackActions } from '@react-navigation/native';
interface props {
  navigation: any,
}
export default class Splash extends Component<props>{
  async componentDidMount() {
    const data = await new Promise(resolve =>
      setTimeout(() => resolve('result'), 3000),
    );
    data &&
      this.props.navigation.dispatch(
        StackActions.replace('Home')
      );
  }

  render() {
    return (
      <ImageBackground
        style={{ flex: 1 }}
        source={require("../../assets/bg.jpeg")}
      >
        <View style={{
          justifyContent: 'center',
          alignItems: 'center',
          flex: 1,
        }}>
          <Text style={{ color: '#fff', fontSize: 40 }}>Weather app</Text>
        </View>


      </ImageBackground>
    );
  }
}

const styles = StyleSheet.create({
  contianer: {
    backgroundColor: 'white',
    flex: 1,
    flexDirection: 'column',
    justifyContent: 'center',
    alignItems: 'center',
  },
});
