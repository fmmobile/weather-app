
import React from "react";

import Home from "./screens/Home/index";
import Splash from "./screens/splash/Splash";

import {NavigationContainer} from '@react-navigation/native';
import {createStackNavigator} from '@react-navigation/stack';
const Stack = createStackNavigator();
const screenOptions = {animationEnabled: true, headerShown: false};

const App = () => {
  return (
 <NavigationContainer>
    <Stack.Navigator screenOptions={screenOptions}>
      <Stack.Screen name="Splash" component={Splash} />
      <Stack.Screen name="Home" component={Home} />
    </Stack.Navigator>
  </NavigationContainer>
  );
};

export default App;
