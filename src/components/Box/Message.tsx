import React, { Component }  from 'react'
import {
    StyleSheet,
    View,
    Text
} from 'react-native'


  interface props {
    message?: string;
  }
  
  const BxMessage = (props: props) => {

        let {message}= props
        return (
        <View style={styles.container}>
            <View style={styles.messageContainer}>
                <Text>{message}</Text>
            </View>
        </View>
        )

}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
        padding: 16,
    },
    messageContainer: {
        backgroundColor: 'white',
        padding: 12,
        borderRadius: 10,
    }
})

export default BxMessage