import React, { Component } from 'react'
import {
    ImageBackground,
    StyleSheet
} from 'react-native'
let day = require('../../assets/day.png')
let night = require('../../assets/night.png')

interface props {
    isMood: boolean,
    children: React.ReactNode,
}

const ImContainer = (props: props) => {
    let { isMood } = props;
    return (
        <ImageBackground
            source={isMood ? day : night}
            style={styles.container}
            resizeMode='stretch'
        >
            {props.children}
        </ImageBackground>
    )
}

const styles = StyleSheet.create({
    container: {
        width: '100%',
        height: '100%'
    },
})

export default ImContainer