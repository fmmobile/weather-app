import React from 'react'
import {
    StyleSheet,
    View,StyleProp,ImageStyle,ViewStyle,
    Text,
    Image,
} from 'react-native'


    interface props {
        messageContainer?: any;
        message?: any;
        containerStyle?:StyleProp<ImageStyle>,
        isFirst?:any,
        marginLeft?:any,
        isLast?:any,
        value2?:any,
        image?:any,
        title?:string,
        value1?:any,
        mainContainerStyles?:StyleProp<ViewStyle>
      }
      
 
const CdImage = (props: props) => {

    const mainContainerStyles = [styles.container]

    return (
        <View style={[
            mainContainerStyles,
            props.containerStyle
        ]}>
            <Text style={styles.text1}>{props.title}</Text>
            <Image
                source={{ uri: props.image }}
                style={styles.image}
                resizeMode='contain'
            />
            <View style={[
                styles.text2Container,
                props.value2 && { justifyContent: 'space-between' }
            ]}>
                <Text style={styles.text2}>{props.value1}</Text>
                {
                    props.value2 &&
                    <Text style={styles.text2}>{props.value2}</Text>
                }
            </View>

        </View>
    )
}


const styles = StyleSheet.create({
    container: {
        padding: 12,
        justifyContent: 'space-between',
        alignItems: 'center',
        backgroundColor: 'white',
        borderRadius: 12,
        marginHorizontal: 3,
    },
    text1: {
        fontSize: 14,
    },
    image: {
        width: 37,
        height: 37,
        marginVertical: 3,
    },
    text2: {
        fontSize: 14,
        color: 'gray'
    },
    text2Container: {
        width: '100%',
        flexDirection: 'row',
        justifyContent: 'center'
    }
})

export default CdImage