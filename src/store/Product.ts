import { observable, action } from "mobx";
import Global from '../Globle'
export default class ProductStore {
    @observable public list = [] as  any;
  
    @action
    getItemList() {
        let url ="https://fakestoreapi.com/products?limit=10";
        Global.apiClient(url).then((res)=>{
            this.list=res.data
        }).catch((err)=>{console.log(err)}) 
    }
    
  
    


}

