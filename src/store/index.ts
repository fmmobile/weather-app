
import HomeStore from "./HomeStore";

class StoreRoot {
  public homeStore = new HomeStore();
}

export default new StoreRoot();
